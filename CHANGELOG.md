# Changelog

All notable changes to `guanguans/pipeline` will be documented in this file.

## 1.0.0 - 2021-06-01

* Initial release.
